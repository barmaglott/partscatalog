package ru.barmaglott.service;

import ru.barmaglott.domain.Part;

import java.util.List;
import java.util.Map;

public interface CatalogService {
  List<Part> findAll();
  List<Part> findByCriteria(Map<String, Object> criterias);
}
