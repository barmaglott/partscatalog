package ru.barmaglott.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.barmaglott.config.ApplicationProperties;
import ru.barmaglott.dao.PartDao;
import ru.barmaglott.dao.PartDaoImpl;
import ru.barmaglott.dao.QueryBuilder;
import ru.barmaglott.domain.Part;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class CatalogServiceImp implements CatalogService {
    private static final Logger logger = LoggerFactory.getLogger(CatalogServiceImp.class);

    private QueryBuilder queryBuilder;
    private PartDao partDao;

    public CatalogServiceImp() {
        this.queryBuilder = new QueryBuilder();
        this.partDao = new PartDaoImpl(new ApplicationProperties());
    }

    @Override
    public List<Part> findAll() {
        logger.info("Query all records from table");
        return partDao.find(queryBuilder.createQuery());
    }

    @Override
    public List<Part> findByCriteria(Map<String, Object> criterias) {
        logger.info("Query records from table with conditions");
        return partDao.find(queryBuilder.createQuery(criterias));
    }
}
