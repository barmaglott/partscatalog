package ru.barmaglott.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Класс содержит методы обработки http запроса
 * </p>
 */
public class RequestHandler {
    private static final Logger logger = LoggerFactory.getLogger(RequestHandler.class);

    /**
     * <p>
     * Метод разбора http запроса {@link HttpServletRequest}
     * </p>
     * <p>
     * Метод пытается получить все параметры запроса(POST)
     * и преобразовать переданные значения в map, где key - имя параметра, а value его значение.
     * </p>
     *
     * @param request - запрос
     * @return - представление переданных параметров запроса,
     * если в запросе параметров не переданно возвращается пустая map {@link Collections#emptyMap()}
     */
    public <K, V> Map<K, V> parse(HttpServletRequest request) {
        logger.info("Start parse httpRequest");
        try {
            request.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error("Charset UTF-8 not set to request", e);
        }
        Enumeration<String> requestEnum = request.getParameterNames();
        logger.info("Request enum with parameters is empty [{}]", requestEnum == null);
        if (requestEnum != null) {
            Map<K, V> requestMap = new HashMap<>();
            while (requestEnum.hasMoreElements()) {
                K key = (K) requestEnum.nextElement();
                V value = (V) request.getParameter((String) key);
                logger.info("Parse parameters with name [{}] is [{}]", key, value);
                if (value != null && !((String) value).isEmpty())
                    requestMap.put(key, value);
            }
            return requestMap;
        } else {
            return Collections.emptyMap();
        }
    }
}
