package ru.barmaglott.web;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.barmaglott.domain.Part;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

public class ResponseHandler {
    private static final Logger logger = LoggerFactory.getLogger(ResponseHandler.class);

    public void createOutput(String output, HttpServletResponse response) {
        setTextContentType(response);
        getWriter(response).println(output);
    }

    public void createOutput(Serializable pojo, HttpServletResponse response) {
        setJsonContentType(response);
//        createJson(getWriter(response), pojo);
        getWriter(response).println(createJson(pojo));
    }

    public void createOutputList(List<Part> pojoList, HttpServletResponse response){
        setJsonContentType(response);
        createJson(getWriter(response), pojoList);
    }

    public void createOutput(List<Part> pojoList, HttpServletResponse response) {
        createOutputList(pojoList, response);
        /*for (Serializable pojo: pojoList)
            createOutput(pojo, response);*/
    }

    public <K, V> void createOutput(Map<K, V > output, HttpServletResponse response) {
        setTextContentType(response);
        PrintWriter writer = getWriter(response);
        for (K key: output.keySet()){
            writer.print(key + " : ");
            writer.println(output.get(key));
        }
    }

    private void createJson(Writer writer, List<Part> pojo) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
        try {
            mapper.writeValue(writer, pojo);
            logger.info("Return json for front");
        } catch (JsonProcessingException exception) {
            logger.error("Error create json for response", exception);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String createJson(Serializable pojo) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
        try {
            return mapper.writeValueAsString(pojo);
        } catch (JsonProcessingException exception) {
            logger.error("Error create json for response", exception);
            return "{\"output\": \"error\", \"text\": \"error\"}";
        }
    }

    private PrintWriter getWriter(HttpServletResponse response) {
        try {
            return response.getWriter();
        } catch (IOException  exception) {
            System.out.println(exception);
            return getWriter(response);
        }
    }


    private void setTextContentType(HttpServletResponse response){
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
    }

    private void setJsonContentType(HttpServletResponse response) {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.setStatus(200);
    }

    private void setXmlContentType(HttpServletResponse response) {
        response.setContentType("application/xml");
        response.setCharacterEncoding("UTF-8");
    }

}
