package ru.barmaglott.web;

import org.h2.jdbcx.JdbcConnectionPool;
import org.h2.tools.RunScript;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;

public class InitEmbeddedDBServlet {

    public void init(){
        JdbcConnectionPool cp = JdbcConnectionPool.create("jdbc:h2:~/test", "sa", "sa");
        File script_ddl = new File(getClass().getResource("/db/h2/schema.sql").getFile());
        File script_dml = new File(getClass().getResource("/db/h2/data.sql").getFile());
        try (Connection connection = cp.getConnection()) {
            RunScript.execute(connection, new FileReader(script_ddl));
            RunScript.execute(connection, new FileReader(script_dml));
        } catch (SQLException | FileNotFoundException e) {
            e.printStackTrace();
        }
        cp.dispose();
    }
}
