package ru.barmaglott.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.barmaglott.service.CatalogService;
import ru.barmaglott.service.CatalogServiceImp;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

public class CatalogServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(CatalogServlet.class);

    private RequestHandler requestHandler;
    private ResponseHandler responseHandler;
    private CatalogService catalogService;



    public CatalogServlet() {
        new InitEmbeddedDBServlet().init();
        this.requestHandler = new RequestHandler();
        this.responseHandler = new ResponseHandler();
        this.catalogService = new CatalogServiceImp();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
//        requestHandler.parse(request);
//        responseHandler.createOutput("Hello world", response);
        responseHandler.createOutput(catalogService.findAll(), response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) {
//        responseHandler.createOutput(requestHandler.parse(request), response);
        responseHandler.createOutput(catalogService.findByCriteria(requestHandler.<String, Object>parse(request)), response);
    }
}
