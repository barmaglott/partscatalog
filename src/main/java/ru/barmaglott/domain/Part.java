package ru.barmaglott.domain;

import java.io.Serializable;
import java.util.Date;

public class Part implements Serializable {
    @Ignore
    private static final long serialVersionUID = -517418786228215780L;

    private String name;
    private String number;
    private String vendor;
    private int qty;
    private Date shipped;
    private Date receive;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Date getShipped() {
        return shipped;
    }

    public void setShipped(Date shipped) {
        this.shipped = shipped;
    }

    public Date getReceive() {
        return receive;
    }

    public void setReceive(Date receive) {
        this.receive = receive;
    }
}
