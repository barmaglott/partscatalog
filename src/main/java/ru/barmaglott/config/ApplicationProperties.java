package ru.barmaglott.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ApplicationProperties {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationProperties.class);
    /**
     * <p>
     *     Файл свойств доступа к БД
     * </p>
     */
    private static final String DB_PROPERTIES = "config/db.properties";

    private static String dbDriver;
    private static String dbUser;
    private static String dbPassword;

    public ApplicationProperties() {
//        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
//        String dbConfigPath = ClassLoader.getSystemClassLoader().getResource(DB_PROPERTIES).getPath();
//        String dbConfigPath = "/config/" +"db.properties";
//        String catalogConfigPath = rootPath + "catalog";

        try {
            String dbConfigPath = Thread.currentThread().getContextClassLoader().getResource(DB_PROPERTIES).getPath();
            Properties dbProps = new Properties();
            dbProps.load(new FileInputStream(dbConfigPath));
            this.dbDriver = dbProps.getProperty("driver");
            this.dbUser = dbProps.getProperty("user");
            this.dbPassword = dbProps.getProperty("password");
        } catch (IOException e) {
            logger.error("Error create ApplicationProperties.class", e);
        }

//        Properties catalogProps = new Properties();
//        catalogProps.load(new FileInputStream(catalogConfigPath));
    }

    public static String getDbDriver() {
        return dbDriver;
    }

    public static String getDbUser() {
        return dbUser;
    }

    public static String getDbPassword() {
        return dbPassword;
    }
}
