package ru.barmaglott.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QueryWithCriteria implements Query {
    private static final Logger logger = LoggerFactory.getLogger(QueryWithCriteria.class);

    private boolean isDate;
    private boolean isAfter;
    private boolean isNumber;

    private final String criteriaName;
    private final Object criteriaValue;
    private Query query;

    public QueryWithCriteria(String criteriaName, Object criteriaValue) {
        this.criteriaName = criteriaName;
        this.criteriaValue = criteriaValue;
    }

    @Override
    public Query getQueryWithCriteria() {
        return this.query;
    }

    @Override
    public void setQueryWithCriteria(Query queryWithCriteria) {
        this.query = queryWithCriteria;
    }

    /**
     * <p>
     * Имплементация
     * </p>
     *
     * @return
     */
    @Override
    public String getQuery() {
        logger.info("Method getQuery() start with params criteriaName [{}], criteriaValue [{}], innerQuery [{}]",
                criteriaName, criteriaValue, getQueryWithCriteria());
        String query = TABLE_ALIASE + POINT +
                createColumnName(criteriaName) +
                createCondition(criteriaValue) +
                (getQueryWithCriteria() != null ? " AND " + getQueryWithCriteria().getQuery() : SEMICOLON);
        logger.info("Part query for data base [{}]", query);
        return query;
    }

    /**
     * <p>
     * Разбор переданного условия
     * </p>
     * <p>
     * Разбирается два типа строка({@link String}) и число({@link Number})
     * Дата должа быть передана строкой
     * (определяется на этапе формирования именования столбца для поиска)
     * </p>
     *
     * @param criteriaValue - условие поиска
     * @return - сформированое для вставки в запрос условие
     * @throws IllegalArgumentException - в случае если переданное условие не соответствует требуемым типам
     *                                  {@link Number}, {@link String}
     */
    private String createCondition(Object criteriaValue) {
        logger.info("Condition is [{}]", criteriaValue);
        if (criteriaValue instanceof String) {
            if (isDate) {
                if (isAfter) {
                    return " > " + APOSTROPHE + criteriaValue + APOSTROPHE;
                } else {
                    return " < " + APOSTROPHE + criteriaValue + APOSTROPHE;
                }
            } else if (isNumber) {
                logger.info("Condition type is number");
                return " >= " + criteriaValue;
            } else {
                logger.info("Condition type is string");
                return " LIKE " + APOSTROPHE + "%" + criteriaValue + "%" + APOSTROPHE;
            }
        } else if (criteriaValue instanceof Number) {
            logger.info("Condition type is number");
            return " >= " + criteriaValue;
        }
        logger.error("Not parse type condition");
        throw new IllegalArgumentException("Type condition is not parse");
    }

    /**
     * <p>
     * Преобразование параметра поиска в название столбца
     * </p>
     * <p>
     * Побочным эфектом является определение
     * является ли переданный параметр датой и условие поиска (до\после)
     * </p>
     *
     * @param criteriaName - параметр поиска
     * @return название столбца в таблице
     */
    private String createColumnName(String criteriaName) {
        String date = "date.";
        String after = ".after";
        String before = ".before";
        if (criteriaName.startsWith(date)) {
            logger.info("Condition type is date");
            isDate = true;
            String result = criteriaName.substring(criteriaName.indexOf(date) + date.length());
            if (criteriaName.endsWith(after)) {
                logger.info("Date is after");
                isAfter = true;
                return PREFIX + (result.substring(0, result.indexOf(after)));
            } else {
                logger.info("Date is before");
                return PREFIX + (result.substring(0, result.indexOf(before)));
            }
        } else if (criteriaName.equals("qty")) {
            isNumber = true;
        }
        return PREFIX + criteriaName;

    }
}
