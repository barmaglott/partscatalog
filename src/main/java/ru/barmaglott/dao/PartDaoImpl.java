package ru.barmaglott.dao;

import org.h2.jdbcx.JdbcConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.barmaglott.config.ApplicationProperties;
import ru.barmaglott.domain.Ignore;
import ru.barmaglott.domain.Part;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PartDaoImpl implements PartDao {
    private static final Logger logger = LoggerFactory.getLogger(PartDaoImpl.class);
    private static final String PREFIX = "PART_";

    private JdbcConnectionPool connectionPool;

    public PartDaoImpl(ApplicationProperties applicationProperties) {
        logger.info("Create PartDaoImpl");
        this.connectionPool = JdbcConnectionPool.create(
                applicationProperties.getDbDriver(),
                applicationProperties.getDbUser(),
                applicationProperties.getDbPassword());
    }

    @Override
    public List<Part> find(Query query) {
        try (Connection connection = connectionPool.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query.getQuery());
            return createPartList(resultSet);
        } catch (SQLException |IllegalAccessException e) {
            logger.error("Exception", e);
        }
        return Collections.emptyList();
    }

    private List<Part> createPartList(ResultSet resultSet) throws SQLException, IllegalAccessException {
        List<Part> result = new ArrayList<>();
        Field[] fields = Part.class.getDeclaredFields();
        while (resultSet.next()) {
            Part part = new Part();
            for (Field field : fields) {
                if (field.isAnnotationPresent(Ignore.class)) continue;
                field.setAccessible(true);
                field.set(part, resultSet.getObject(PREFIX + field.getName().toUpperCase()));
            }
            result.add(part);
        }
        return result;
    }

    protected void finalize() {
        connectionPool.dispose();
    }
}
