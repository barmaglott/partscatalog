package ru.barmaglott.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * <p>
 * Класс содержит логику создания объектов запроса к БД
 * </p>
 */
public class QueryBuilder {
    private static final Logger logger = LoggerFactory.getLogger(QueryBuilder.class);

    /**
     * <p>
     * Если условий для запроса не переданно,
     * возвращается объект запроса,
     * содержащий запрос всех записей
     * </p>
     *
     * @return - запрос всех записей {@link SelectAllQuery}
     */
    public Query createQuery() {
        return new SelectAllQuery();
    }

    /**
     * <p>
     * Метод имплементирует создание запроса к БД содержащий условия
     * </p>
     * <p>
     * Создается связный список объектов (по количеству переданных услловий)
     * </p>
     *
     * @param criterias - список условий для поиска
     * @return - связный список, каждый элемент которого это условие
     */
    public Query createQuery(Map<String, Object> criterias) {
        if (criterias.isEmpty()) return createQuery();
        logger.info("Size conditions map for select [{}]", criterias.size());
        Query query = new SelectQuery();
        Query oldQuery = query;
        for (Map.Entry<String, Object> entry : criterias.entrySet()) {
            Query newQuery = new QueryWithCriteria(entry.getKey(), entry.getValue());
            oldQuery.setQueryWithCriteria(newQuery);
            oldQuery = newQuery;
        }
        return query;
    }
}
