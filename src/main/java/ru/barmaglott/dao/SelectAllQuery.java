package ru.barmaglott.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Класс позволяет создать объект запроса всех записей из БД
 * </p>
 */
public class SelectAllQuery implements Query {
    private static final Logger logger = LoggerFactory.getLogger(SelectAllQuery.class);

    private static final String QUERY = "SELECT * FROM PARTS " + TABLE_ALIASE + SEMICOLON;

    /**
     * <p>
     * Объект класса должен создаваться builder {@link QueryBuilder}
     * </p>
     */
    SelectAllQuery() {
    }

    /**
     * <p>
     * Не подразумевается создание дополнительных условий
     * </p>
     *
     * @return - null
     */
    @Override
    public Query getQueryWithCriteria() {
        return null;
    }

    /**
     * <p>
     * Не подразумевается создание дополнительных условий
     * </p>
     *
     * @param queryWithCriteria - дополнительное условие
     * @throws IllegalStateException - объекту пытаются придать несвойственное ему состояние
     */
    @Override
    public void setQueryWithCriteria(Query queryWithCriteria) {
        throw new IllegalStateException("Object SelectAllQuery.class must not have internal conditions");
    }


    /**
     * <p>
     * Метод возвращает запрос для БД
     * </p>
     *
     * @return - запрос для БД
     */
    @Override
    public String getQuery() {
        logger.info("Query for data base [{}]", QUERY);
        return QUERY;
    }
}
