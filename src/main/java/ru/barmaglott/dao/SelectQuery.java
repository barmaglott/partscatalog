package ru.barmaglott.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SelectQuery implements Query {
    private static final Logger logger = LoggerFactory.getLogger(SelectQuery.class);

    private static final String QUERY = "SELECT * FROM PARTS " + TABLE_ALIASE ;
    private Query queryWithCriteria;

    SelectQuery() {
    }

    @Override
    public Query getQueryWithCriteria() {
        return queryWithCriteria;
    }

    @Override
    public void setQueryWithCriteria(Query queryWithCriteria) {
        this.queryWithCriteria = queryWithCriteria;
    }

    @Override
    public String getQuery() {
        String query = QUERY + (getQueryWithCriteria() != null ? " WHERE " + getQueryWithCriteria().getQuery() : SEMICOLON);
        logger.info("Query for data base [{}]", query);
        return query;
    }
}
