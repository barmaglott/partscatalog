package ru.barmaglott.dao;

/**
 * <p>
 * Интерфейс обязателен для всех имплементаций запроса
 * </p>
 */
public interface Query {
    /**
     * <p>
     * Алиас таблицы
     * </p>
     */
    String TABLE_ALIASE = "p";
    /**
     * <p>
     * Перфикс всех столбцов таблицы
     * </p>
     */
    String PREFIX = "part_";

    String APOSTROPHE = "\'";
    String SEMICOLON = " ;";
    String POINT = ".";

    Query getQueryWithCriteria();

    void setQueryWithCriteria(Query queryWithCriteria);

    /**
     * <p>
     * Запрос для базы данных
     * </p>
     *
     * @return - запрос
     */
    String getQuery();
}
