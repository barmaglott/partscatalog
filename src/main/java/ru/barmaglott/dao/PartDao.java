package ru.barmaglott.dao;

import ru.barmaglott.domain.Part;

import java.util.Date;
import java.util.List;

public interface PartDao {
    List<Part> find(Query query);
}
