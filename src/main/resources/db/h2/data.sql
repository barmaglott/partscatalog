INSERT
  INTO parts(part_name, part_number, part_vendor, part_qty, part_shipped, part_receive)
  VALUES ('HPC Blade 7', '56H212-D1', 'CH-DAL', 64, '2007-02-06', '2007-03-06');
INSERT
  INTO parts(part_name, part_number, part_vendor, part_qty, part_shipped, part_receive)
  VALUES ('HPC Blade 8', '56H212-D2', 'CH-DAL', 65, '2017-02-06', '2017-03-06');
INSERT
  INTO parts(part_name, part_number, part_vendor, part_qty, part_shipped, part_receive)
  VALUES ('HPC Blade 8', '56H212-D2', 'CH-DAL', 60, '2017-02-07', '2017-03-06');
INSERT
  INTO parts(part_name, part_number, part_vendor, part_qty, part_shipped, part_receive)
  VALUES ('HPC Red 3', '56D101-A1', 'CH-VAN', 14, '2011-02-15', '2012-01-12');