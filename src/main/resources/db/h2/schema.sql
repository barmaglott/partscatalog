DROP TABLE IF EXISTS parts;

CREATE TABLE IF NOT EXISTS parts (
id INT NOT NULL AUTO_INCREMENT,
part_name VARCHAR(20) NOT NULL , -- именование
part_number VARCHAR(20) NOT NULL , -- номер
part_vendor VARCHAR(20) NOT NULL, -- поставщик
part_qty INT ,
part_shipped DATE ,
part_receive DATE,

PRIMARY KEY(id)
);