package ru.barmaglott.dao;

import org.h2.jdbcx.JdbcConnectionPool;
import org.h2.tools.RunScript;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.barmaglott.config.ApplicationProperties;
import ru.barmaglott.domain.Part;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.assertNotNull;

public class QueryTest {
    private PartDao partDao;

    @BeforeMethod(groups = "testQuery")
    public void init() throws IOException {
        partDao = new PartDaoImpl(new ApplicationProperties());
    }

    @Test
    public void testConnection() {
        JdbcConnectionPool cp = JdbcConnectionPool.create("jdbc:h2:~/test", "sa", "sa");
        File script_ddl = new File(getClass().getResource("/db/h2/schema.sql").getFile());
        File script_dml = new File(getClass().getResource("/db/h2/data.sql").getFile());
        try (Connection connection = cp.getConnection()) {
            RunScript.execute(connection, new FileReader(script_ddl));
            RunScript.execute(connection, new FileReader(script_dml));
        } catch (SQLException | FileNotFoundException e) {
            e.printStackTrace();
        }
        cp.dispose();
    }

    @Test(dependsOnMethods = "testConnection", groups = "testQuery")
    public void testQuerySelectAll() {
        for (Part part : partDao.find(new QueryBuilder().createQuery())) {
            assertNotNull(part.getName());
           /* System.out.println(
                    "NAME: " + part.getName() +
                            "--NUMBER: " + part.getNumber() +
                            "--VENDOR: " + part.getVendor() +
                            "--QTY: " + part.getQty() +
                            "--SHIPPED: " + part.getShipped() +
                            "--RECEIVE: " + part.getReceive());*/
        }
    }

    @Test(dependsOnMethods = "testConnection", groups = "testQuery")
    public void testQuerySelectByString(){
        Query query = new QueryBuilder().createQuery(Collections.<String, Object>singletonMap("name", "Blade"));
        for (Part part: partDao.find(query)) {
            assertNotNull(part.getName());
            /*System.out.println(
                    "NAME: " + part.getName() +
                    "--NUMBER: " + part.getNumber() +
                    "--VENDOR: " + part.getVendor() +
                    "--QTY: " + part.getQty() +
                    "--SHIPPED: " + part.getShipped() +
                    "--RECEIVE: " + part.getReceive());*/
        }
    }

    @Test(dependsOnMethods = "testConnection", groups = "testQuery")
    public void testQuerySelectBySeveralStringConditions(){
        Map<String, Object> criterias = new HashMap<>();
        criterias.put("name", "Blade");
        criterias.put("number", "D2");
        Query query = new QueryBuilder().createQuery(criterias);
        for (Part part: partDao.find(query)) {
            assertNotNull(part.getName());
           /* System.out.println(
                    "NAME: " + part.getName() +
                            "--NUMBER: " + part.getNumber() +
                            "--VENDOR: " + part.getVendor() +
                            "--QTY: " + part.getQty() +
                            "--SHIPPED: " + part.getShipped() +
                            "--RECEIVE: " + part.getReceive());*/
        }
    }

    @Test(dependsOnMethods = "testConnection", groups = "testQuery")
    public void testQuerySelectByNumber(){
        Query query = new QueryBuilder().createQuery(Collections.<String, Object>singletonMap("qty", 15));
        for (Part part: partDao.find(query)) {
            assertNotNull(part.getName());
            /*System.out.println(
                    "NAME: " + part.getName() +
                            "--NUMBER: " + part.getNumber() +
                            "--VENDOR: " + part.getVendor() +
                            "--QTY: " + part.getQty() +
                            "--SHIPPED: " + part.getShipped() +
                            "--RECEIVE: " + part.getReceive());*/
        }
    }

    @Test(dependsOnMethods = "testConnection", groups = "testQuery")
    public void testQuerySelectByStringAndNumberConditions(){
        Map<String, Object> criterias = new HashMap<>();
        criterias.put("name", "Blade");
        criterias.put("number", "D2");
        criterias.put("qty", 65);
        Query query = new QueryBuilder().createQuery(criterias);
        for (Part part: partDao.find(query)) {
            assertNotNull(part.getName());
          /*  System.out.println(
                    "NAME: " + part.getName() +
                            "--NUMBER: " + part.getNumber() +
                            "--VENDOR: " + part.getVendor() +
                            "--QTY: " + part.getQty() +
                            "--SHIPPED: " + part.getShipped() +
                            "--RECEIVE: " + part.getReceive());*/
        }
    }

    @DataProvider
    public Object[][] dataQuerySelectByDate(){
        return new Object[][]{
                {"date.shipped.after", "2011-02-14"},
                {"date.shipped.before", "2011-02-16"},
                {"date.receive.after","2012-01-11"},
                {"date.receive.before","2012-01-11"}
        };
    }
    @Test(dependsOnMethods = "testConnection", groups = "testQuery", dataProvider = "dataQuerySelectByDate")
    public void testQuerySelectByDate(String key, String value){
        Query query = new QueryBuilder().createQuery(Collections.<String, Object>singletonMap(key, value));
        for (Part part: partDao.find(query)) {
            assertNotNull(part.getName());
           /* System.out.println(
                    "NAME: " + part.getName() +
                            "--NUMBER: " + part.getNumber() +
                            "--VENDOR: " + part.getVendor() +
                            "--QTY: " + part.getQty() +
                            "--SHIPPED: " + part.getShipped() +
                            "--RECEIVE: " + part.getReceive());*/
        }
    }

    @Test(dependsOnMethods = "testConnection", groups = "testQuery")
    public void testQuerySelectBySeveralDate(){
        Map<String, Object> criterias = new HashMap<>();
        criterias.put("date.shipped.after", "2007-02-06");
        criterias.put("date.shipped.before", "2017-02-07");
        criterias.put("date.receive.before", "2015-01-25");
        Query query = new QueryBuilder().createQuery(criterias);
        for (Part part: partDao.find(query)) {
            assertNotNull(part.getName());
           /* System.out.println(
                    "NAME: " + part.getName() +
                            "--NUMBER: " + part.getNumber() +
                            "--VENDOR: " + part.getVendor() +
                            "--QTY: " + part.getQty() +
                            "--SHIPPED: " + part.getShipped() +
                            "--RECEIVE: " + part.getReceive());*/
        }
    }


}
