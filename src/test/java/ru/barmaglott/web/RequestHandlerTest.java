package ru.barmaglott.web;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class RequestHandlerTest {
    private HttpServletRequest httpServletRequest;
    private RequestHandler requestHandler;

    @BeforeClass
    public void init() {
        httpServletRequest = mock(HttpServletRequest.class);
        requestHandler = new RequestHandler();
    }

    @Test
    public void testParseWithNullParametrs() {
        requestHandler.parse(httpServletRequest);
    }

    @Test
    public void testParse() {
        boolean flag = true;
        String stringKey = "testStringKey";
        String stringValue = "testStringValue";
        String intKey = "testIntKey";
        int intValue = 111;
        String dateKey = "testDateKey";
        Date dateValue = new Date();
        List<String> keyList = new ArrayList<>(Arrays.asList(new String[]{stringKey, intKey, dateKey}));
        Enumeration<String> requestEnum = mock(Enumeration.class);

//        when(requestEnum.hasMoreElements()).thenReturn(flag);
        when(requestEnum.nextElement()).thenReturn(stringKey);
//        flag = false;
        when(httpServletRequest.getParameterNames()).thenReturn(requestEnum);
        when(httpServletRequest.getParameter(stringKey)).thenReturn(stringValue);

        Map<String, Object> requestMap = requestHandler.parse(httpServletRequest);

//        assertEquals(requestMap.get(stringKey), stringValue);
    }
}
